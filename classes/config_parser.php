<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../lib.php');

/**
 * Interface for accessing config file information.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
interface iltisource_ecohub_config_info {
    /**
     * Get the full filename including path for the config file found during initialization.
     * @return string|null
     */
    public function get_full_file_name();
    /**
     * Get the file modified timestamp for the config file.
     * @return int|null
     */
    public function get_last_modified_stamp();
}

/**
 * Interface for parsing config file information.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
interface iltisource_ecohub_config_parser extends iltisource_ecohub_config_info{
    /**
     * Load config file from storage
     * @throw file_exception
     */
    public function load_config_file();
    /**
     * Parse config JSON.
     * @param object $jsono The config JSON object to parse.
     */
    public function parse_config_file($jsono);
    /**
     * Get loaded config settings.
     * @return array|null
     */
    public function get_settings();
}

/**
 * Class for resolving config file information.
 *
 * Searches for config.json in local config directory and in parent directories if not found.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_config_info implements iltisource_ecohub_config_info
{
    protected $defaultpath;
    protected $configfilename;

    /**
     * Locate config file. If not file name is given we look for config.json in the /config directory.
     *
     * @param string $configfilename Optional full path of config file.
     * @throws file_exception.
     * @throws coding_exception.
     */
    public function __construct($configfilename = '') {
        if (empty($configfilename)) {
            $this->defaultpath = realpath(dirname(__FILE__) . '/../config');
            $this->configfilename = $this->locate_config_file($this->defaultpath);
        } else {
            $this->configfilename = $configfilename;
        }
    }

    /**
     * Get the path and file name of resolved config file.
     * @return string Full path and name of config file.
     */
    public function get_full_file_name() {
        return $this->configfilename;
    }

    /**
     * Get the modified timestamp of resolved config file.
     * @return int File modifed timestamp (cached).
     */
    public function get_last_modified_stamp() {
        try {
            $stamp = filemtime($this->configfilename); // The filemtime caches the stamp.
        } catch (Exception $e) {
            try {
                $this->configfilename = $this->locate_config_file($this->defaultpath);
            } catch (Exception $e) {
                // Impossible to find config file.
                return false;
            }
            return $this->get_last_modified_stamp();
        }
        return $stamp;
    }

    /**
     * Search for a config.json in currentpath and traverse through parent dirs.
     * @param string $currentpath Start place.
     * @return bool|string True if successful otherwise error string.
     * @throws coding_exception
     * @throws file_exception
     */
    protected function locate_config_file($currentpath) {
        $file = '';
        // Look for config.json.
        if (is_dir($currentpath)) {
            if ($handle = opendir($currentpath)) {
                while ($file = readdir($handle)) {
                    if (strtolower($file) == 'config.json') {
                        break;
                    }
                }
            }
            closedir($handle);
        }
        // Otherwise look in current directory.
        if ($file === '' || $file === false) {
            if ($currentpath !== '.') {
                return $this->locate_config_file('.');
            }
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_config_not_found', 'ltisource_ecohub'));
        }

        $file = $currentpath . DIRECTORY_SEPARATOR . $file;
        if (!file_exists($file)) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_file_not_found', 'ltisource_ecohub', $file));
        }
        return $file;
    }
}

/**
 * Interface for passing config parser initialization parameters.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_config_parser_parameters {
    public function __construct() {
        $this->configfilename = '';
        $this->pluginrelease = null;
    }

    public static function with_config_file_name($configfilename) {
        $instance = new self();
        $instance->configfilename = $configfilename;
        return $instance;
    }

    public static function with_plugin_release($pluginrelease) {
        $instance = new self();
        $instance->pluginrelease = $pluginrelease;
        return $instance;
    }

    public $configfilename;
    public $pluginrelease;
}

/**
 * Class for parsing config file content.
 *
 * Parses the config file content and throws file_exception of mandatory settings are invalid.
 * Unknown settings are ignored.
 * This is an extension class of ltisource_ecohub_config_info.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_config_parser extends ltisource_ecohub_config_info implements iltisource_ecohub_config_parser
{
    protected $settings = null;
    protected $filebasedsettings = null;
    protected $pluginrelease = null;

    /**
     * Store current plugin release.
     *
     * @param ltisource_ecohub_config_parser_parameters $parameters Optional full path of config file or current plugin release.
     * @throws file_exception.
     * @throws coding_exception.
     */
    public function __construct(ltisource_ecohub_config_parser_parameters $parameters) {
        $this->pluginrelease = $parameters->pluginrelease;

        parent::__construct($parameters->configfilename);
    }

    /**
     * Load and parse the resolved config file.
     *
     * @throws coding_exception
     * @throws file_exception If file cannot be opened or parsed.
     */
    public function load_config_file() {
        if (empty($this->pluginrelease)) {
            throw new coding_exception('release is not initialized');
        }
        if (!file_exists($this->configfilename)) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_file_not_found', 'ltisource_ecohub', $this->configfilename));
        }

        $jsons = false;
        clearstatcache();
        $file = fopen($this->configfilename, "r");
        if ($file !== false) {
            if (flock($file, LOCK_SH)) {
                fseek($file, 0, SEEK_END);
                $filesize = ftell($file);
                fseek($file, 0);
                $jsons = fread($file, $filesize);
                fclose($file);
            }
        }
        if ($file === false || $jsons === false) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_file_not_json', 'ltisource_ecohub', $this->configfilename));
        }
        $this->parse_config_file(array_change_key_case(json_decode($jsons, true)));
    }

    /**
     * Parse a config file JSON object.
     *
     * @param object|array The config JSON object to be parsed.
     * @throws file_exception If file cannot be parsed.
     * @throws coding_exception
     */
    public function parse_config_file($jsono) {
        if (is_a($jsono, 'stdClass')) {
            $jsono = json_decode(json_encode($jsono), true);
        }
        if ($jsono === null) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_file_not_json', 'ltisource_ecohub', $this->configfilename));
        }
        if (!isset($jsono['version'], $jsono['as_domain'])) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_missing_fields', 'ltisource_ecohub', $this->configfilename));
        }

        $jsonver = $jsono['version'];
        $matches = array();
        if (preg_match('/(\d+)(\.)(\d+)/', $this->pluginrelease, $matches) === 1) {
            $pluginrelease = $matches[0];
        } else {
            $pluginrelease = '1.0';
        }
        if (floatval($jsonver) > floatval($pluginrelease)) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_file_newer', 'ltisource_ecohub',
                            (object) array('filename' => $this->configfilename,
                                    'newversion' => $jsonver,
                                    'supportedversion' => $pluginrelease)));
        }
        $this->filebasedsettings['version'] = $jsonver;
        if (isset($jsono['siteid'])) {
            $this->filebasedsettings['siteid'] = $jsono['siteid'];
        }

        $jsonasurl = $this->validate_url($jsono['as_domain'], $this->configfilename);
        $this->filebasedsettings['asdomain'] = $jsonasurl;
        if (isset($jsono['readonly'])) {
            $this->filebasedsettings['readonly'] = $jsono['readonly'];
        } else {
            $this->settings['readonly'] = true;
        }
        if (isset($jsono['activation'])) {
            $this->filebasedsettings['activation'] = $jsono['activation'];
        } else {
            $this->settings['activation'] = false;
        }
        if (isset($jsono['cs_domain'])) {
            $this->filebasedsettings['csdomain'] = $this->validate_url($jsono['cs_domain'], $this->configfilename);
        } else {
            $this->settings['csdomain'] = '';
        }

        $jsonltimap = $this->load_array($jsono, 'lti_map');
        if (!empty($jsonltimap)) {
            $this->filebasedsettings['ltimap'] = $jsonltimap;
        }
        $jsonallowedlti = $this->load_array($jsono, 'allowed_lti_params');
        if (!empty($jsonallowedlti)) {
            $this->filebasedsettings['allowedltiparams'] = $jsonallowedlti;
        }
        $jsonvalidlti = $this->load_array($jsono, 'validation_lti_params');
        if (!empty($jsonvalidlti)) {
            $this->filebasedsettings['validationltiparams'] = $jsonvalidlti;
        }
    }

    /**
     * Get all the latest settings, loaded and parsed.
     * @return array The parsed configuration.
     */
    public function get_settings() {
        if (empty($this->settings)) {
            return $this->filebasedsettings;
        }
        if (empty($this->filebasedsettings)) {
            return $this->settings;
        }
        return array_merge($this->settings, $this->filebasedsettings);
    }

    /**
     * Get latest settings from file.
     * @return array The loaded configuration.
     */
    public function get_filebasedsettings() {
        return $this->filebasedsettings;
    }

    /**
     * Validate launch url and enforce https.
     * @param string $url.
     * @param string $configfilename Full config file path for error information.
     * @return string Validated url.
     * @throws coding_exception.
     * @throws file_exception.
     */
    public function validate_url($url, $configfilename = '') {
        if ($url === '') {
            return '';
        }
        $urlelem = parse_url($url);
        if ($urlelem === false || (!isset($urlelem['host']) && !isset($urlelem['path']))) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_asdomain_invalid', 'ltisource_ecohub', $configfilename));
        }
        $host = isset($urlelem['host']) ? $urlelem['host'] : '';
        $port = isset($urlelem['port']) ? ':' . $urlelem['port'] : '';
        $path = isset($urlelem['path']) ? $urlelem['path'] : '';
        $query = isset($urlelem['query']) ? '?' . $urlelem['query'] : '';
        $fragment = isset($urlelem['fragment']) ? '#' . $urlelem['fragment'] : '';
        // Enforce SSL.
        $newurl = 'https://' . $host . $port . $path . $query . $fragment;

        if (!filter_var($newurl, FILTER_VALIDATE_URL)) {
            throw new file_exception(get_string('config_error_title', 'ltisource_ecohub'),
                    get_string('config_error_asdomain_invalid', 'ltisource_ecohub', $configfilename));
        }
        return $newurl;
    }

    public function config_equals_loaded($config, $loaded = null) {
        if (is_a($config, 'stdClass')) {
            $config = json_decode(json_encode($config), true);
        }
        if (empty($loaded)) {
            $loaded = $this->filebasedsettings;
        }
        $jsoniterator = new \IteratorIterator(new \ArrayIterator($config));
        foreach ($jsoniterator as $key => $value) {
            if (!array_key_exists($key, $loaded)) {
                $key = str_replace('_', '', $key);
                if (!array_key_exists($key, $loaded)) {
                    return false;
                }
            }
            if (is_array($value)) {
                if (!is_array($loaded[$key]) || !$this->config_equals_loaded($value, $loaded[$key])) {
                    return false;
                }
            } else {
                if ($value !== $loaded[$key]) {
                    try {
                        $correctedurl = $this->validate_url($value);
                    } catch (Exception $e) {
                        return false;
                    }
                    if ($correctedurl !== $loaded[$key]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected function is_valid_property($property) {
        if ($property === '') {
            return true;
        }
        $spos = strpos($property, '{');
        if ($spos === false) {
            return true;
        }
        if ($spos > 0) {
            return $this->is_valid_property(substr( $property, $spos));
        }
        $epos = strpos($property, '}');
        if ($epos !== false) {
            $validproperties = array(
                'var.organizationname',
                'user'
            );
            foreach ($validproperties as $vp) {
                if (substr($property, 1, strlen($vp)) === $vp) {
                    return $this->is_valid_property(substr($property, $epos + 1));
                }
            }
        }
        return false;
    }

    protected function load_array($jsono, $name) {
        $jsonarray = array();
        if (isset($jsono[$name])) {
            $jsoniterator = new IteratorIterator(
                    new ArrayIterator($jsono[$name]));
            foreach ($jsoniterator as $ltiparam => $property) {
                if (is_array($property)) {
                    $jsonarray[$ltiparam] = $this->load_array($jsono[$name], $ltiparam);
                } else if (!isset($jsonarray[$ltiparam]) && $this->is_valid_property($property)) {
                    if (is_int($ltiparam)) {
                        array_push($jsonarray, $property);
                    } else {
                        $jsonarray[$ltiparam] = $property;
                    }
                }
            }
        }
        return $jsonarray;
    }
}
