<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Interface for accessing configuration items.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
interface iltisource_ecohub_storage {
    /**
     * Get configuration item or all of them.
     * @param string|null $key The key for the configuration item. Null to return all.
     * @return string|array|false Return configuration item(s). False on error.
     */
    public function get($key = null);
    /**
     * Set a configuration item.
     * @param string $key The key for the configuration item.
     * @param string|int|bool $value The configuration item value.
     * @param bool Validate value if true (default).
     * @return bool True on success.
     */
    public function set($key, $value, $validate = true);
    /**
     * Purge all configuration items.
     * @return bool True on success.
     */
    public function purge();
    /**
     * Get time of last configuration item change.
     * @return int Time stamp.
     */
    public function get_last_modified();
}

/**
 * Interface for accessing extended configuration items.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
interface iltisource_ecohub_ext_storage extends iltisource_ecohub_storage {
    /**
     * Get time of last configuration item change.
     * @param int Time stamp.
     */
    public function set_last_modified($timestamp);
    /**
     * Get current plugin release version string.
     * @return string Current plugin release version.
     */
    public function get_plugin_release();
    /**
     * Set the current plugin release version.
     * @param string Plugin release string.
     */
    public function set_plugin_release($release);
}

/**
 * Base class for config setting validation.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_base_storage {
    protected $settings = array(
        'version',
        'siteid',
        'asdomain',
        'readonly',
        'activation',
        'csdomain',
        'ltimap',
        'allowedltiparams',
        'validationltiparams'
    );

    /**
     * Check of setting is valid.
     * @param string $key Configuration item name.
     * @return bool True if valid.
     */
    public function is_valid_key($key) {
        // Reserved keys start with an underscore.
        return in_array($key, $this->settings) || substr($key, 0, 1) === '_';
    }

    protected function validate_setting($key, $value) {
        if ($this->is_valid_key($key)) {
            if ($key === 'readonly' || $key === 'activation') {
                $value = $value ? 1 : 0;
            } else if (is_array($value)) {
                // Split arrays into multiple rows to prevent exceeding row size.
                $value = array();
                $index = '[' . $key . '].';
                foreach ($value as $tkey => $tvalue) {
                    $value[$index . $tkey] = $tvalue;
                }
            }
            return $value;
        }
        return false;
    }

    protected function validate_all(array $keyvalues, callable $keymapperfn = null) {
        $validated = array();
        foreach ($keyvalues as $key => $value) {
            if ($keymapperfn !== null) {
                $key = $keymapperfn($key);
            }
            $validatedvalue = $this->validate_setting($key, $value);
            if ($validatedvalue !== false) {
                $validated[$key] = $validatedvalue;
            }
        }
        return !empty($validated) ? $validated : false; // Everything validated.
    }
}

/**
 * Extension class for managing configuration items in file storage.
 *
 * Support reading of settings only.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_file_storage extends ltisource_ecohub_base_storage implements iltisource_ecohub_storage {
    protected $parser;
    protected $configfileinfocache;

    /**
     * Constructor.
     *
     * Use dependency injection for parsing file content and accessing the config file information.
     *
     * @param iltisource_ecohub_config_parser $parser Parser object for parsing file content.
     * @param cache_application $cache Cache object to obtain file information.
     */
    public function __construct(iltisource_ecohub_config_parser $parser, cache_application $cache) {
        $this->parser = $parser;
        $this->configfileinfocache = $cache;
    }

    /**
     * Get configuration item or all of them from file storage.
     *
     * @param string|null $key The key for the configuration item. Null to return all.
     * @return string|array|false Return configuration item(s). False on error.
     */
    public function get($key = null) {
        $settings = $this->parser->get_settings();
        if (empty($settings) || ($key !== null && !array_key_exists($key, $settings))) {
            // Always try to reload.
            try {
                $this->parser->load_config_file();
            } catch (Exception $e) {
                return false;
            }
            $settings = $this->parser->get_settings();
        }
        if (!empty($settings)) {
            $settings = $this->deserialize($settings);
            if (empty($key)) {
                return $settings;
            }
            if ($this->is_valid_key($key) && array_key_exists($key, $settings)) {
                return $settings[$key];
            }
        }
        // Try file info if everything fails.
        return $this->configfileinfocache->get($key);
    }

    /**
     * Set a configuration item in file storage.
     *
     * Not supported in v1.0.
     *
     * @param string $key The key for the configuration item.
     * @param string|int|bool $value The configuration item value.
     * @param bool Validate value if true (default).
     * @return bool True on success.
     */
    public function set($key, $value, $validate = true) {
        return false; // Readonly for now.

    }

    /**
     * Purge all configuration items in file storage.
     *
     * Not supported in v1.0.
     *
     * @return bool True on success.
     */
    public function purge() {
        return false; // Readonly for now.
    }

    /**
     * Get time of last file change.
     *
     * @return int Time stamp.
     */
    public function get_last_modified() {
        return $this->parser->get_last_modified_stamp();
    }

    /**
     * Deserialize complex settings such as arrays.
     *
     * @param array $settings Array of serialized settings.
     * @return array|false Return deserialized settings. False on error.
     */
    public function deserialize($settings) {
        $data = array();
        foreach ($settings as $key => $value) {
            if ($this->is_valid_key($key)) {
                if (is_bool($value)) {
                    $value = +$value;
                }
                $data[$key] = $value;
            }
        }
        return $data;
    }
}

/**
 * Extension class for managing configuration items in database storage.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_db_storage extends ltisource_ecohub_base_storage implements iltisource_ecohub_ext_storage
{
    protected $moodleconfigname = 'mod_lti_source_ecohub';

    /**
     * Get configuration item or all of them from database.
     *
     * @param string|null $key The key for the configuration item. Null to return all.
     * @return string|array|false Return configuration item(s). False on error.
     */
    public function get($key = null) {
        $settings = $this->db_get_table();

        if (!empty($settings)) {
            $settings = $this->deserialize($settings);
            if (empty($key)) {
                return $settings;
            }
            if ($this->is_valid_key($key) && array_key_exists($key, $settings)) {
                return $settings[$key];
            }
        }

        return false;
    }

    /**
     * Set a configuration item in database.
     *
     * @param string|array $key The key for the configuration item.
     * @param string|int|bool $value The configuration item value.
     * @param bool Validate value if true (default).
     * @return bool True on success.
     */
    public function set($key, $value = null, $validate = true) {

        $settings = is_array($key) ? $key : array($key => $value);
        foreach ($settings as $key => $value) {
            if ($validate) {
                $value = $this->serialize($key, $value);
                $key = $this->get_db_key($key);
                if ($value === false || $key === false) {
                    return false;
                }
            }
            // Split array into rows.
            if (is_array($value)) {
                foreach ($value as $akey => $avalue) {
                    if ($this->db_set_row($akey, $avalue) === false) {
                        return false;
                    }
                }
            } else if ($this->db_set_row($key, $value) === false) {
                return false;
            }
        }
        return true;
    }

    public function purge() {
        global $DB;

        // Delete from config_plugins.
        try {
            $DB->delete_records('config_plugins', array('plugin' => $this->moodleconfigname));
        } catch (dml_exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Get time of last database change.
     *
     * @return int Time stamp.
     */
    public function get_last_modified() {
        return $this->get('_lastmodified');
    }

    /**
     * Get time of last configuration item change.
     * @param int Time stamp.
     */
    public function set_last_modified($timestamp) {
        $this->db_set_row('_lastmodified', $timestamp);
    }

    /**
     * Get current plugin release version string.
     * @return string Current plugin release version.
     */
    public function get_plugin_release() {
        return $this->get('_pluginrelease');
    }

    /**
     * Set the current plugin release version.
     * @param string Plugin release string.
     */
    public function set_plugin_release($release) {
        $this->db_set_row('_pluginrelease', $release);
    }

    /**
     * Serialize complex settings such as arrays.
     *
     * @param string $key Configuration item name.
     * @param string|array $value The setting to serialize.
     * @return string|false Return serialized setting. False on error.
     */
    public function serialize($key, $value = null) {
        $result = false;
        if (is_array($key)) {
            $result = array();
            foreach ($key as $skey => $svalue) {
                $result[$skey] = $this->serialize($skey, $svalue);
            }
        } else if ($this->is_valid_key($key)) {
            if (is_bool($value)) {
                $result = $value ? 1 : 0;
            } else if (is_array($value)) {
                // Split first level arrays into multiple rows to prevent exceeding row size.
                $data = array();
                $index = '[' . $key . '].';
                foreach ($value as $tkey => $tvalue) {
                    if (is_array($tvalue)) {
                        // Serialize value arrays.
                        $data[$index . $tkey] = json_encode($tvalue, JSON_HEX_APOS | JSON_HEX_QUOT);
                    } else {
                        $data[$index . $tkey] = $tvalue;
                    }
                }
                $result = $data;
            } else {
                $result = $value;
            }
        }
        return $result;
    }

    /**
     * Deserialize complex settings such as arrays.
     *
     * @param array $settings Array of serialized settings.
     * @return array|false Return deserialized settings. False on error.
     */
    public function deserialize($settings) {
        $data = array();
        foreach ($settings as $key => $value) {
            $cleankey = $this->get_from_db_key($key);
            if ($cleankey !== false && $this->is_valid_key($cleankey)) {
                $data[$cleankey] = $value;
            } else if ($key[0] === '[') {
                // Serialized key.
                $pos = strpos($key, '].', 1);
                if ($pos !== false && $pos > 1) {
                    $cleankey = substr($key, 1, $pos - 1);
                    $key = substr($key, $pos + 2);
                    if (strpos($value, '[') === 0) {
                        // Deserialize value if it is an array.
                        $value = json_decode($value);
                    }
                    if (array_key_exists($cleankey, $data)) {
                        $data[$cleankey][$key] = $value;
                    } else {
                        $data[$cleankey] = array($key => $value);
                    }
                }
            }
        }
        return $data;
    }

    protected function get_db_key($key) {
        $interpreted = false;
        if ($this->is_valid_key($key)) {
            $interpreted = 'setting' . $key;
        }
        return $interpreted;
    }

    protected function get_from_db_key($key) {
        $original = false;
        if (substr($key, 0, 1) === '_') {
            $original = $key;
        } else if (strlen($key) > 7) {
            $key = substr($key, 7);
            if ($this->is_valid_key($key)) {
                $original = $key;
            }
        }
        return $original;
    }

    protected function db_get_table() {
        global $DB;

        try {
            $settings = $DB->get_records_menu('config_plugins',
                    array('plugin' => $this->moodleconfigname), '',
                    'name,value');
        } catch (dml_exception $e) {
            return false;
        }
        return $settings;
    }

    protected function db_set_row($key, $value) {
        global $DB;

        try {
            if ($id = $DB->get_field('config_plugins',
                    'id',
                    array('name' => $key, 'plugin' => $this->moodleconfigname))) {
                if ($value === null) {
                    $DB->delete_records('config_plugins', array('name' => $key, 'plugin' => $this->moodleconfigname));
                } else {
                    $DB->set_field('config_plugins', 'value', $value, array('id' => $id));
                }
            } else {
                if ($value !== null) {
                    $config = new stdClass();
                    $config->plugin = $this->moodleconfigname;
                    $config->name = $key;
                    $config->value = $value;
                    $DB->insert_record('config_plugins', $config, false);
                }
            }
        } catch (dml_exception $e) {
            return false;
        }
        return true;
    }
}