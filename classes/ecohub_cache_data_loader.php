<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once('storage.php');
require_once('mapping.php');

/**
 * Data loader class for configuration settings cache.
 *
 * This is essentially a cache wrapper for configuration settings for easy access to the settings.
 * This is an extension class of cache_application.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ecohub_settings_cache_data_loader extends cache_application {
    protected $filestorage = null;
    protected $lastfilestamp = null;
    protected $dbstorage = null;

    /**
     * Overrides the cache construct method.
     *
     * @param cache_definition $definition
     * @param cache_store $store
     * @param cache_loader|cache_data_source $loader
     * @throws file_exception.
     * @throws coding_exception.
     */
    public function __construct(cache_definition $definition, cache_store $store, $loader = null) {
        $this->dbstorage = new ltisource_ecohub_db_storage();
        $release = $this->dbstorage->get_plugin_release();
        if (empty($release) || gettype($release) !== 'string') {
            // If there is no release version in db, it must be a v1.0, so use v1.1.
            $release = 'v1.1';
            $this->dbstorage->set_plugin_release($release);
        }
        $this->filestorage = new ltisource_ecohub_file_storage(
            new ltisource_ecohub_config_parser(ltisource_ecohub_config_parser_parameters::with_plugin_release($release)),
            cache::make('ltisource_ecohub', 'configfile'));

        parent::__construct($definition, $store, $loader);
    }

    /**
     * Retrieves the value for the given key from the cache.
     *
     * @param string|int $key The key for the data being requested.
     * @param int $strictness One of IGNORE_MISSING | MUST_EXIST
     * @return mixed|false The data from the cache or false if the key did not exist within the cache.
     */
    public function get($key, $strictness = IGNORE_MISSING) {
        $value = false;
        if ($this->lastfilestamp === null) {
            // First time call after init and config file might also be changed.
            $dbdataset = $this->dbstorage->get();
            if (empty($dbdataset) ||
                    !array_key_exists('_lastmodified', $dbdataset) ||
                    $dbdataset['_lastmodified'] < $this->filestorage->get_last_modified()) {
                $this->copy_file_config_to_db($dbdataset !== false, false);
            } else {
                parent::set_many($dbdataset);
                if (array_key_exists('_lastmodified', $dbdataset)) {
                    $this->lastfilestamp = $dbdataset['_lastmodified'];
                }
            }
            cache_helper::purge_by_event('ecohubconfigchanged');
            $value = parent::get($key);
        } else if ($this->lastfilestamp < $this->filestorage->get_last_modified()) {
            // Config file has changed so we need to reload.
            $this->copy_file_config_to_db(true, true);
        }
        // Return value if cached.
        if ($value === false) {
            $value = parent::get($key, $strictness);
        }
        // Otherwise load from db and cache it.
        if ($value === false) {
            $value = $this->dbstorage->get($key);
            if ($value !== false) {
                parent::set($key, $value);
            }
        }
        return $value;
    }

    /**
     * Retrieves an array of values for an array of keys or all data.
     *
     * @param array|null $keys The keys of the data being requested.
     *      Each key can be any structure although using a scalar string or int is recommended in the interests of performance.
     *      In advanced cases an array may be useful such as in situations requiring the multi-key functionality.
     * @param int $strictness One of IGNORE_MISSING or MUST_EXIST.
     * @return array An array of key value pairs for the items that could be retrieved from the cache.
     */
    public function get_many(array $keys, $strictness = IGNORE_MISSING) {
        if (empty($keys)) {
            return $this->dbstorage->get();
        }
         $return = array();
        foreach ($keys as $key) {
            $return[$key] = $this->get($key, $strictness);
        }
        return $return;
    }

    /**
     * Sends a key => value pair to the cache.
     *
     * @param string|int $key The key for the data being requested.
     * @param mixed $data The data to set for the key.
     * @return bool True on success, false otherwise.
     */
    public function set($key, $data) {
        $ecohubmap = ltisource_ecohub_mapping::get_instance();
        if (is_array($data)) {
            foreach ($data as $param => $mapentry) {
                if ($ecohubmap->expand_map_entry($mapentry) === false) {
                    return false;
                }
            }
        } else if ($ecohubmap->expand_map_entry($data) === false) {
            return false;
        }
        if ($this->dbstorage->set($key, $data) === false) {
            return false;
        }
        if (is_array($data)) {
            $rootdata = parent::get($key);
            foreach ($data as $param => $mapentry) {
                $rootdata[$param] = $mapentry;
            }
            parent::set($key, $rootdata);
        } else {
            parent::set($key, $data);
        }
        $this->lastfilestamp = time();
        $this->dbstorage->set_last_modified($this->lastfilestamp);
        return true;
    }

    /**
     * Sends several key => value pairs to the cache.
     *
     * @param array $keydataarray An array of key => data pairs to write to cache.
     * @return int The number of items successfully set.
     */
    public function set_many(array $keydataarray) {
        $result = 0;

        if ($this->dbstorage->set($keydataarray) === true) {
            $jsoniterator = new IteratorIterator(new ArrayIterator($keydataarray));
            foreach ($jsoniterator as $key => $value) {
                if (is_array($value)) {
                    $rootdata = parent::get($key);
                    foreach ($value as $param => $mapentry) {
                        $rootdata[$param] = $mapentry;
                    }
                    parent::set($key, $rootdata);
                } else {
                    parent::set($key, $value);
                }
                $result++;
            }
            $this->lastfilestamp = time();
            $this->dbstorage->set_last_modified($this->lastfilestamp);
        }
        return $result;
    }

    /**
     * Purges the cache store, and loader if there is one.
     *
     * @return bool True on success, false otherwise
     */
    public function purge() {
        if (parent::purge()) {
            // Retain the plugin release. This ought to be in a separate table.
            $pluginrelease = $this->dbstorage->get_plugin_release();
            $this->dbstorage->purge();
            $this->dbstorage->set_plugin_release($pluginrelease);
            $this->lastfilestamp = null;
        }
        return true;
    }

    protected function copy_file_config_to_db($purgedb = false, $purgecache = false) {
        $filedataset = $this->filestorage->get();
        if ($filedataset !== false) {
            $pluginrelease = $this->dbstorage->get_plugin_release();
            if ($purgedb) {
                $this->dbstorage->purge();
            }
            if (!isset($filedataset['siteid'])) {
                $filedataset['siteid'] = $this->create_site_id();
            }
            $this->dbstorage->set($filedataset);
            // Do not overwrite plugin info.
            $this->dbstorage->set_plugin_release($pluginrelease);
            $this->lastfilestamp = $this->filestorage->get_last_modified();
            $this->dbstorage->set_last_modified($this->lastfilestamp);

            if ($purgecache) {
                parent::purge();
            }
            parent::set_many($filedataset);
        }
    }

    protected function create_site_id() {
        global $CFG;
        $parts = null;

        // There was no site ID so we have to invent one. Try parsing the config.php first.
        if (!empty($CFG->httpswwwroot)) {
            $parts = parse_url($CFG->httpswwwroot);
        } else if (!empty($CFG->wwwroot)) {
            $parts = parse_url($CFG->wwwroot);
        }
        if (!empty($parts) &&
                isset($parts['host']) &&
                substr($parts['host'], 0, 9) !== '127.0.0.1') {
            return $parts['host'];
        }
        // Then try to get a nname from the web server.
        if (isset($_SERVER['HTTP_HOST'])) {
            return $_SERVER['HTTP_HOST'];
        }
        if (isset($_SERVER['SERVER_NAME'])) {
            return $_SERVER['SERVER_NAME'];
        }
        // Just invent one then.
        return uniqid('ecohub_');
    }
}