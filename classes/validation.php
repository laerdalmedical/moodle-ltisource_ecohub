<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();
/**
 * Class for validating parameters before launch.
 *
 * Checks if parameter is allowed to change and validation terms are given in "validation_lti_params" then perform
 * the validation.
 *
 * Example in configuration:
 *   "validation_lti_params": {
 *      "custom_any": ["is_min_max_len", 1, 64]
 *   }
 *
 * Will check if the "custom_any" LTI parameter is at least one character long and no more than 64.
 * Pre-defined validation terms are given in validationltiparams array.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_validation {
    protected static $instance = null;
    protected $allowedltiparams = array(
            'user_id',
            'custom_organization_name',
            'custom_institution_id',
            'custom_institution_name',
            'custom_department_id',
            'custom_department_name',
            'custom_job_title_id',
            'custom_job_title_name',
            'custom_job_category_id',
            'custom_job_category_name',
            'custom_lis_person_timezone',
            'lis_person_name_full'
    );

    protected $validationltiparams = array(
            'oauth_consumer_key' => ['is_start_guid'],
            'user_id' => ['is_min_max_len', 1, 64],
            'custom_organization_name' => ['is_min_max_len', 0, 256],
            'custom_institution_id' => ['is_min_max_len', 1, 64],
            'custom_institution_name' => ['is_min_max_len', 0, 256],
            'custom_department_id' => ['is_min_max_len', 1, 64],
            'custom_department_name' => ['is_min_max_len', 0, 256],
            'custom_job_category' => ['is_min_max_len', 1, 64],
            'custom_job_title' => ['is_min_max_len', 0, 256],
    );

    /**
     * Returns an instance of the validation class.
     *
     * @return object
     */
    public static function get_instance() {
        if (is_null(self::$instance)) {
            self::$instance = new ltisource_ecohub_validation();
        }
        return self::$instance;
    }

    /**
     * Constructor.
     *
     * Use settings cache for settings access and create validation functions.
     */
    public function __construct() {
        $config = cache::make('ltisource_ecohub', 'configsettings');
        $extra = $config->get('allowedltiparams');
        if (!empty($extra)) {
            $this->allowedltiparams = array_merge($this->allowedltiparams, $extra);
        }
        $extra = $config->get('validationltiparams');
        if (!empty($extra)) {
            $this->validationltiparams = array_merge($this->validationltiparams, $extra);
        }
        foreach ($this->validationltiparams as $param => $checker) {
            $count = count($checker);
            if (is_array($checker) && $count > 0) {
                $funcname = array_shift($checker);
                if (method_exists($this, $funcname)) {
                    $this->validationltiparams[$param] = function($value) use ($funcname, $param, $checker) {
                        return $this->$funcname($param, $value, $checker);
                    };
                } else if (function_exists($funcname)) {
                    $this->validationltiparams[$param] = function($value) use ($funcname, $param, $checker) {
                        return $funcname($param, $value, $checker);
                    };
                } else {
                    unset($this->validationltiparams[$param]);
                }
            } else if (!empty($checker)) {
                unset($this->validationltiparams[$param]);
            }
        }
    }

    /**
     * Check if array of LTI parameters are valid.
     *
     * @param array $ltiparams Array of LTI parameters to validate.
     * @return bool True on success.
     * @throws coding_exception.
     */
    public function validate_lti_parameters($ltiparams) {
        foreach ($ltiparams as $param => $value) {
            $result = $this->is_lti_param_allowed($param);
            if ($result !== true) {
                return $result;
            }
            $result = $this->is_lti_param_valid($param, $value);
            if ($result !== true) {
                return $result;
            }
        }
        return true;
    }

    /**
     * Check if an LTI parameter is allowed to be changed.
     *
     * @param string $param LTI parameter.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception.
     */
    public function is_lti_param_allowed($param) {
        if (!in_array($param, $this->allowedltiparams)) {
            return get_string('lti_param_not_allowed_error', 'ltisource_ecohub', ['param' => $param]);
        }
        return true;
    }

    /**
     * Check if an LTI parameters value is valid.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @return true|string True on success, otherwise an error string.
     */
    public function is_lti_param_valid($param, $value) {
        if (array_key_exists($param, $this->validationltiparams)) {
            if (is_callable($this->validationltiparams[$param])) {
                return $this->validationltiparams[$param]($value);
            }
            return false;
        }
        return true;
    }

    /**
     * Check if an LTI parameter is a GUID.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_guid($param, $value) {
        if (empty($value) ||
                strlen($value) !== 36 ||
                preg_match("/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i", $value) === false) {
            return get_string('lti_param_invalid_not_guid_error',
                    'ltisource_ecohub',
                    ['param' => $param, 'value' => $value]);
        }
        return true;
    }

    /**
     * Check if start of an LTI parameter is a GUID.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_start_guid($param, $value) {
        $value = substr($value, 0, 36);
        return $this->is_guid($param, $value);
    }

    /**
     * Check if LTI parameter is within a range.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @param array $args Boundary limits.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_min_max_len($param, $value, $args) {
        $count = count($args);
        if (is_string( $value) && $count === 2) {
            $len = strlen($value);
            if ($len >= $args[0] && $len <= $args[1]) {
                return true;
            }
        }
        $range = $count === 0 ? 'null' : '';
        foreach ($args as $arg) {
            $range .= strlen($range) > 0 ? '..' . $arg : $arg;
        }
        return get_string('lti_param_invalid_not_in_range_error',
                'ltisource_ecohub',
                ['param' => $param, 'value' => $value, 'range' => $range]);
    }

    /**
     * Check if LTI parameter has a minimum length.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @param array $args Boundary limits.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_min_len($param, $value, $args) {
        $count = count($args);
        if (is_string( $value) && $count === 1) {
            $len = strlen($value);
            if ($len >= $args[0]) {
                return true;
            }
        }
        return get_string('lti_param_invalid_not_in_range_error',
                'ltisource_ecohub',
                ['param' => $param, 'value' => $value, 'range' => $args[0]]);
    }

    /**
     * Check if LTI parameter has a maximum length.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @param array $args Boundary limits.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_max_len($param, $value, $args) {
        $count = count($args);
        if (is_string( $value) && $count === 1) {
            $len = strlen($value);
            if ($len <= $args[0]) {
                return true;
            }
        }
        return get_string('lti_param_invalid_not_in_range_error',
                'ltisource_ecohub',
                ['param' => $param, 'value' => $value, 'range' => $args[0]]);
    }

    /**
     * Check if LTI parameter is empty.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_empty($param, $value) {
        if (empty($value)) {
            return true;
        }
        return get_string('lti_param_invalid_error',
                'ltisource_ecohub',
                ['param' => $param, 'value' => $value]);
    }

    /**
     * Check if LTI parameter isn't empty.
     *
     * @param string $param LTI parameter.
     * @param string $value The value of the parameter.
     * @return true|string True on success, otherwise an error string.
     * @throws coding_exception
     */
    protected function is_not_empty($param, $value) {
        if (!empty($value)) {
            return true;
        }
        return get_string('lti_param_invalid_empty_error',
                'ltisource_ecohub',
                ['param' => $param]);
    }
}