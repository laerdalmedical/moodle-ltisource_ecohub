<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once('mapping.php');
require_once('validation.php');

/**
 * Mapping setting in configuration.
 *
 * A field representing one mapping of user profile/system data into an LTI parameter.
 * This is a variation of ecohub_setting_configtext.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ecohub_setting_configmapping extends admin_setting {
    protected $store;
    protected $maproot;
    protected $mapentry;
    protected $mapper;
    protected $validator;

    /**
     * ecohub_setting_configmapping constructor.
     *
     * @param string $name Element name.
     * @throws coding_exception.
     */
    public function __construct($name) {
        $this->mapper = ltisource_ecohub_mapping::get_instance();
        $this->validator = ltisource_ecohub_validation::get_instance();

        $this->store = cache::make('ltisource_ecohub', 'configsettings');
        $this->nosave = $this->is_readonly();

        $parts = explode('/', $name);
        if (count($parts) < 2) {
            $this->maproot = 'ltimap';
            $this->mapentry = $name;
        } else {
            $this->maproot = $parts[0];
            $this->mapentry = $parts[1];
        }

        parent::__construct($this->maproot . $this->mapentry, get_string('settings_param_map_title', 'ltisource_ecohub'), '', null);
    }

    public function is_readonly() {
        return $this->store->get('readonly');
    }

    protected function get_readonly() {
        $retval = null;
        if ($this->is_readonly()) {
            $retval = array( (object) ['name' => 'readonly', 'value' => 'readonly'] );
        }
        return $retval;
    }

    /**
     * Get whether this should be displayed in LTR mode.
     *
     * Try to guess from the PARAM type unless specifically set.
     */
    public function get_force_ltr() {
        $forceltr = parent::get_force_ltr();
        if ($forceltr === null) {
            return !is_rtl_compatible(PARAM_TEXT);
        }
        return $forceltr;
    }

    public function get_setting() {
        return $this->store->get($this->maproot)[$this->mapentry];
    }

    /**
     * @param object $data Setting string.
     * @return mixed|string True on success or error string.
     * @throws coding_exception.
     */
    public function write_setting($data) {
        if ($this->is_readonly()) {
            return false;
        }
        $param = $data['p'];
        $mapping = $data['m'];
        $test = $this->validate($param, $mapping);
        if (!empty($test)) {
            if (array_key_exists('param', $test)) {
                return $test['param'];
            } else if (array_key_exists('mapping', $test)) {
                return $test['mapping'];
            }
        } else {
            if ($this->store->set($this->maproot, array($param => $mapping)) === true) {
                return '';
            };
        }
        return get_string('errorsetting', 'admin');
    }

    /**
     * Return an XHTML string for the setting
     *
     * @param mixed $data array or string depending on setting
     * @param string $query
     * @return string Returns an XHTML string
     * @throws coding_exception
     */
    public function output_html($data, $query='') {
        global $OUTPUT;

        $setting = $this->get_setting();
        $expression = $this->mapper->expand_map_entry($setting);
        $test = $this->validate($this->mapentry, $setting, $expression);
        $paramattributes = $mapattributes = $this->get_readonly();
        $description = '';
        if (!empty($test)) {
            if (array_key_exists('param', $test)) {
                $paramattributes[] = (object) ['name' => 'style', 'value' => 'color:red'];
                $description = $test['param'];
            }
            if (array_key_exists('mapping', $test)) {
                $mapattributes[] = (object) ['name' => 'style', 'value' => 'color:red'];
                $description = $test['mapping'];
            }
        } else {
            $context = $this->create_description_context($this->mapentry, $setting, $expression);
            if ($expression === $setting) {
                $description = get_string('settings_param_map_description_static', 'ltisource_ecohub', $context);
            } else {
                $description = get_string('settings_param_map_description', 'ltisource_ecohub', $context);
            }
        }
        $warning = $expression === false ?
                get_string('settings_param_map_title_warning', 'ltisource_ecohub') : '';
        $context = (object) [
                'name' => $this->get_full_name(),
                'paramvalue' => $this->mapentry,
                'paramsize' => 32,
                'paramattributes' => $paramattributes,
                'mapvalue' => $setting,
                'mapsize' => 32,
                'mapattributes' => $mapattributes,
                'id' => $this->get_id(),
                'forceltr' => $this->get_force_ltr(),
                'to' => get_string('settings_param_map_with', 'ltisource_ecohub')];
        $element = $OUTPUT->render_from_template('ltisource_ecohub/setting_configmapping', $context);

        return format_admin_setting($this, $this->visiblename, $element, $description, true, $warning, null, $query);
    }

    /**
     * @param string $param Parameter name.
     * @param string $mapping Mapping string.
     * @param string $expression Possible evaluated mapping, null by default.
     * @return array.
     * @throws coding_exception.
     */
    protected function validate($param, $mapping, $expression = null) {
        $context = $this->create_description_context($param, $mapping, $expression);
        $result = array();
        if (($test = $this->validator->is_lti_param_allowed($param)) !== true) {
            $result['param'] = $test;
        }
        if ($context->expression === false) {
            $result['mapping'] = get_string('settings_param_map_description_error', 'ltisource_ecohub', $context);
        } else if (($test = $this->validator->is_lti_param_valid($param, $context->expression)) !== true) {
            $result['mapping'] = $test;
        }
        return $result;
    }

    protected function create_description_context($param, $mapping, $expression = null) {
        return (object)[
                'param' => $param,
                'mapping' => $mapping,
                'expression' => $expression ? $expression : $this->mapper->expand_map_entry($mapping)];
    }
}