<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once('config_parser.php');

/**
 * Data source class for configuration file info.
 *
 * Cache loader for configuration file information.
 * This is an extension class of cache_data_source.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ecohub_configfile_cache_data_source implements \cache_data_source {

    protected static $instance = null;
    protected $configfileinfo = null;

    /**
     * Returns an instance of the data source class that the cache can use for loading data using the other methods
     * specified by the cache_data_source interface.
     *
     * @param \cache_definition $definition
     * @return object
     * @throws file_exception.
     * @throws coding_exception.
     */
    public static function get_instance_for_cache(\cache_definition $definition) {
        if (is_null(self::$instance)) {
            self::$instance = new ecohub_configfile_cache_data_source();
        }
        return self::$instance;
    }

    /**
     * ecohub_configfile_cache_data_source constructor.
     *
     * @throws file_exception.
     * @throws coding_exception.
     */
    public function __construct() {
        $this->configfileinfo = new ltisource_ecohub_config_info();
    }

    /**
     * Loads the data for the key provided ready formatted for caching.
     *
     * @param string|int $key The key to load.
     * @return mixed What ever data should be returned, or false if it can't be loaded.
     */
    public function load_for_cache($key) {
        $key = strtolower($key);
        if ($key === 'filename') {
            return $this->configfileinfo->get_full_file_name();
        }
        if ($key === 'timestamp') {
            return $this->configfileinfo->get_last_modified_stamp();
        }
        if ($key === 'isvalid') {
            if (!empty($this->configfileinfo->get_full_file_name()) && $this->configfileinfo->get_last_modified_stamp() !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Loads several keys for the cache.
     *
     * @param array $keys An array of keys each of which will be string|int.
     * @return array An array of matching data items.
     */
    public function load_many_for_cache(array $keys) {
        $results = [];

        foreach ($keys as $key) {
            $results[] = $this->load_for_cache($key);
        }

        return $results;
    }
}
