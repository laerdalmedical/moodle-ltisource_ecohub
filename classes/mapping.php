<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Class for resolving mapping expressions.
 *
 * For example the expression "{user.username}" will resolve to $USER->username.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ltisource_ecohub_mapping {
    protected static $instance = null;
    protected $validmapvars = array();

    /**
     * Returns an instance of the data source class that the cache can use for loading data using the other methods
     * specified by the cache_data_source interface.
     *
     * @return object
     */
    public static function get_instance() {
        if (is_null(self::$instance)) {
            self::$instance = new ltisource_ecohub_mapping();
        }
        return self::$instance;
    }

    /**
     * During instantiation the validmapvars is populated with resolver functions for var and user.
     */
    public function __construct() {
        $this->validmapvars['var'] = function($mapvar) {
            return $this->get_var($mapvar);
        };
        $this->validmapvars['user'] = function($mapvar) {
            return $this->get_user($mapvar);
        };
    }

    /**
     * Add resolver function for an expression.
     *
     * If no functions is given then remove the resolver.
     *
     * @param string $var Resolver expression tag, like var and user.
     * @param object|null $callback Resolving function if being set.
     */
    public function set_map_var($var, $callback = null) {
        if (empty($callback)) {
            unset($this->validmapvars->$var);
        } else {
            $this->validmapvars[$var] = $callback;
        }
    }

    /**
     * Resolve an array of expressions.
     *
     * @param array $mapentries An array of expressions to be resolved.
     * @return array|false A corresponding array of resolved strings. False on error.
     */
    public function get_expanded_settings($mapentries) {
        $expanded = false;
        if ($mapentries !== false) {
            if (is_array($mapentries)) {
                $expanded = array();
                foreach ($mapentries as $mapentry => $mapentrystring) {
                    $mapvalue = $this->expand_map_entry($mapentrystring);
                    if ($mapvalue === false) {
                        return false;
                    }
                    $expanded[$mapentry] = $mapvalue;
                }
            }
        }
        if (empty($expanded)) {
            return false;
        }
        return $expanded;
    }

    /**
     * Resolve an expression.
     *
     * @param string $mapentrystring A single expression to be resolved.
     * @return string|false A corresponding resolved string. False on error.
     */
    public function expand_map_entry($mapentrystring) {
        return $this->parse_string_recursively($mapentrystring);
    }

    protected function parse_string_recursively($mapentrystring, $result = '') {
        if (!empty($mapentrystring)) {
            // Copy ordinary un-tagged text.
            if ($mapentrystring[0] !== '{') {
                $pos = strpos($mapentrystring, '{');
                if ($pos === null) {
                    return false;
                } else if ($pos === false) {
                    // No more substitutions.
                    $result .= $mapentrystring;
                } else {
                    // Copy until next substitution tag.
                    $text = substr($mapentrystring, 0, $pos);
                    $result = $this->parse_string_recursively(substr($mapentrystring, $pos), $result . $text);
                }
            } else {
                // Start substitution.
                $pos = strpos($mapentrystring, '}');
                if (empty($pos)) {
                    // Missing end tag.
                    return false;
                }
                $mapvalue = $this->substitute(substr($mapentrystring, 1, $pos - 1));
                if ($mapvalue === false) {
                    // Syntax error in substitution.
                    return false;
                }
                $result = $this->parse_string_recursively(substr($mapentrystring, $pos + 1), $result . $mapvalue);
            }
        }
        return $result;
    }

    protected function substitute($mapvar) {
        $pos = strpos($mapvar, '.');
        if ($pos === false) {
            $pos = strlen($mapvar);
        }
        foreach ($this->validmapvars as $var => $mapper) {
            if (strncmp($mapvar, $var, $pos) === 0) {
                return $mapper(substr($mapvar, $pos + 1));
            }
        }
        return false;
    }

    protected function get_var($mapvar) {
        if ($mapvar === 'organizationname') {
            try {
                return get_site()->shortname;
            } catch (Exception $e) {
                return false;
            }
        }
        return false;
    }

    protected function get_user($mapvar) {
        global $USER;
        return $this->get_recursive($mapvar, $USER);
    }

    protected function get_recursive($mapvar, $parent) {
        if (!is_object($parent)) {
            try {
                $parent = (object)$parent;
            } catch (Exception $e) {
                return false;
            }
        }
        // See if this is nested.
        $npos = strpos($mapvar, '.');
        // Return if this is the end or go another step down the rabbit hole.
        if ($npos === false && isset($parent->$mapvar) && is_string($parent->$mapvar)) {
            return $parent->$mapvar;
        } else if ($npos !== false) {
            $propname = substr($mapvar, 0, $npos);
            if (isset($parent->$propname)) {
                return $this->get_recursive(substr($mapvar, $npos + 1), $parent->$propname);
            }
        }
        return false;
    }
}