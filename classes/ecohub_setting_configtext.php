<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Config setting in configuration.
 *
 * This field is an extension of configtext with the addition that it can be in a read-only state.
 *
 * @copyright 2018 Laerdal Medical A/S
 * @author Martin Rieva
 * @package ecohub
 */
class ecohub_setting_configtext extends admin_setting_configtext {

    protected $store;
    /**
     * Config text constructor
     *
     * @param string $name unique ascii name,
     * @param string $visiblename localised
     * @param string $description long localised info
     * @param string $defaultsetting
     * @param mixed $paramtype int means PARAM_XXX type, string is a allowed format in regex
     * @param int $size default field size
     */
    public function __construct($name, $visiblename, $description, $defaultsetting, $paramtype = PARAM_RAW, $size = null) {
        parent::__construct($name, $visiblename, $description, $defaultsetting, $paramtype, $size);
        $this->store = cache::make('ltisource_ecohub', 'configsettings');
        $this->nosave = $this->store->get('readonly');
    }

    public function readonly($setting = null) {
        if (!empty($setting)) {
            $this->nosave = $setting;
        }
        return $this->nosave;
    }

    protected function get_readonly() {
        $retval = null;
        if ($this->readonly()) {
            $retval = array( (object) ['name' => 'readonly', 'value' => 'readonly'] );
        }
        return $retval;
    }

    public function get_setting() {
        return $this->store->get($this->name);
    }

    /**
     * @param string $data Setting string.
     * @return mixed|string True on success or error string.
     * @throws coding_exception
     */
    public function write_setting($data) {
        if ($this->readonly()) {
            return '';
        }
        if ($this->paramtype === PARAM_INT and $data === '') {
            // Do not complain if '' used instead of 0.
            $data = 0;
        }
        // ...$data is a string.
        $validated = $this->validate($data);
        if ($validated !== true) {
            return $validated;
        }
        return ($this->store->set($this->name, $data) ? '' : get_string('errorsetting', 'admin'));
    }

    /**
     * Return an XHTML string for the setting
     *
     * @param mixed $data array or string depending on setting
     * @param string $query
     * @return string Returns an XHTML string
     */
    public function output_html($data, $query='') {
        global $OUTPUT;

        $default = $this->get_defaultsetting();
        $context = (object) [
        'size' => $this->size,
        'id' => $this->get_id(),
        'name' => $this->get_full_name(),
        'value' => $data,
        'forceltr' => $this->get_force_ltr(),
        'attributes' => $this->get_readonly()
        ];
        $element = $OUTPUT->render_from_template('admin/setting_configtext', $context);

        return format_admin_setting($this, $this->visiblename, $element, $this->description, true, '', $default, $query);
    }
}