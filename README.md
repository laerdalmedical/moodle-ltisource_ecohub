# Laerdal Medical plugin for Moodle #

This plugin enables Moodle 3.0+ teachers and admins to create Laerdal Medical activities that is launched externally on the Laerdal Medical EcoHub platform.

## Introduction ##

As this is an LTI Source type plugin, hence an LTI sub-plugin, it adds support for creating external activities launched using the LTI standard on the Laerdal Medical E-learning platform called EcoHub.
Although EcoHub accepts standard LTI launch parameters, using this plugin will enable full analytics support for tracking user compliance, metrics etc.for Laerdal Medical customers.

### How it works ###

The EcoHub platform adheres to LTI-1p0 standards (see https://www.imsglobal.org/specs/ltiv1p0). In order to provide explicit analytics data for users, departments, institutions and organizations, several LTI parameters must be present and some must even be unique. Most of these parameters are custom parameters.

When a student start a Laerdal Medical activity the LTI parameters are modified by this plugin just before the launch, providing additional information in the LTI custom parameters.
This information can either be static data, data obtained from the Moodle installation or even user profile details.

Version 1.1 of this plugin has an auto-update feature that is invoked once per day. In case changes are rolled out on the EcoHub backend, configurations are automatically updated accordingly.

### Privacy ###

As the information provided to Laerdal Medical may be obtained from the Moodle user profiles, administrators should be aware of the plugin configuration. The configuration is determined per installation and by default, no user specific data is sent to Laerdal Medical.
The administrator can see the exact mappings from site or user profile data to LTI parameters on the settings page of the plugin. This page also displays sample data for each mapping.

## Installation ##

To install this plugin manually, the files must be copied into a folder with an arbitrary name and be zipped and submitted on the Moodle plugin administration page.
The general installation procedure is described here: [docs.moodle.org](https://docs.moodle.org/en/Installing_plugins)

By default the plugin configuration is nearly empty and customization for a particular organization will be needed.
Please contact a Laerdal Medical sales representative in order to determine the configuration needed for your use.
In case a configuration has been created previously an update can be forced by triggering the auto-update job called _EcoHub LTI Source plugin configuration updater_ on the scheduled tasks administration page.

### Uninstalling ###

If Moodle has been configured correctly the plugin can be uninstalled though the "View All Plugins" page in the administration section.

**Please note:** Uninstalling the plugin will also remove any configuration from the database including updates obtained by the auto-update feature.

## Configuration ##

The plugin is configured using the /config/config.json as base configuration. This file must be in JSON format. When ever this file is modified the configuration is reset and any customizations are removed.
Some minimal settings must be present for the plugin to load properly and additional customization is possible.

Example of a config.json:

```
{
    "version": "1.1",
    "as_domain": "moodle-service.contentservice.net/v1/config",
    "ltimap": {
        "user_id": "{user.username}",
        "custom_organization_name": "{var.organizationname}",
        "custom_institution_id": "{user.profile.inst_id}",
        "custom_lis_person_timezone": "Denmark/Copenhagen"
    }
}
```


### Base configuration ###

A minimal configuration is required in order for the plugin to load properly and it consists of two attribute-pairs:

```
"version": "1.1",
"as_domain": "moodle-service.contentservice.net/v1/config"
```

The _version_ attribute indicates which plugin version this configuration format adheres to, thus, this version must always be less or equal to the plugin version.

The _as_domain_ value must be an URL to the EcoHub Access Service from where configuration updates can be fetched.

In addition to those mandatory settings several customized settings can be added to the configuration.
This can be done either in the configuration file or by using the administrative UI in Moodle. As settings added in the UI are lost when the configuration file is changed, adding these to the configuration file instead has an advantage.
If the _readonly_ attribute in config.json is _true_ (default) the settings cannot be altered within the UI.

### Customized configuration ###

Further configuration can be done by a Moodle administrator on the plugin settings page.
In the plugin settings page the settings from the configuration file can be viewed and changed if the _readonly_ attribute in config.json has the value _false_.

#### Predefined attributes/settings ####

| Attribute | Description | Default value |
| --------- | ----------- | ------------- |
| siteid    | If not provided an attempt of generating a site ID is done. This ID is submitted to the EcoHub Access Service in order to identify the installation and return an up-to-date configuration. | |
| activation | Currently not used | false |
| ltimap | A set of attribute-value pairs that inject LTI parameters during launch. See description below. | empty |
| allowedltiparams | An array of additional LTI parameters allowed to be injected before launch. This setting is only available in the configuration file. See description below. | empty |
| validationltiparam | An array of additional LTI parameter validation done before an activity launch | empty |

#### ltimap ####

This is the definition of the LTI parameters injected during a launch and controls the main purpose of the plugin.
By default it only contains a single entry, but typically multiple entries must be provided in order to populate the LTI launch with sufficient parameters for analytics purposes.

The _attribute_ part contain the name of the LTI parameter to inject. Prepend "custom_" in order to add custom parameters.

The _value_ part contains either static data, substitution data or a mix hereof.

Static data is entered as an ordinary string. In order to insert data for substitution the data path and value must be surrounded by curly braces {}.
The data path has a punctuation character appended and may consist of multiple segments.

In the above example the _custom_lis_person_timezone_ attribute contains only static data, in this case a static timezone string.
When launching an activity the _custom_lis_person_timezone_ LTI parameter is always set to "Denmark/Copenhagen".

The example also show the three data paths available, _var_, _user_ and _user.profile_.

The _var_ data path is predefined and contain only the single value _organizationname_ for now.
Adding _{var.organizationname}_ will substitute the Moodle short site name into the LTI parameter.

The _user_ data path is equivalent to the data available in the global _$USER_ Moodle object. In the example above the _$USER->username_ is placed in the _user_id_ LTI parameter.
The _profile_ object is a member of $USER, so the _user.profile_ path will access data available for each users profile. As these can be defined per Moodle site they are fully customizable.
In the example above a profile entry named "inst_id" has been added to the site and the string is placed in the _custom_institution_id_ LTI parameter.

If substitution data does not exist the plugin will throw an error during the launch of the activity.

#### allowedltiparams ####

This can contain an array of additional LTI parameters allowed to be changed during the activity launch.
By default only the following LTI parameters can be changed:

* user_id
* lis_person_name_full
* custom_organization_name
* custom_institution_id
* custom_institution_name
* custom_department_id
* custom_department_name
* custom_job_title_id
* custom_job_title_name
* custom_job_category_id
* custom_job_category_name
* custom_lis_person_timezone

For example, in the hypothetically case a _custom_snapchat_id_ must be used, it must be added to the config:

```
"allowedltiparams" : [
    "custom_snapchat_id"
],
"ltimap": {
    "custom_snapchat_id": "{user.profile.snapid}",
}
```

#### validationltiparams ####

As the LTI parameters are substituted just before the launch of an activity some validation of the substituted data may apply.
This can be defined by adding rules to the _validationltiparams_ array.

By default the following rules apply:

* oauth_consumer_key: ['is_start_guid']
* user_id: ['is_min_max_len', 1, 64]
* custom_organization_name: ['is_min_max_len', 0, 256]
* custom_institution_id: ['is_min_max_len', 1, 64]
* custom_institution_name: ['is_min_max_len', 0, 256]
* custom_department_id: ['is_min_max_len', 1, 64]
* custom_department_name: ['is_min_max_len', 0, 256]
* custom_job_category: ['is_min_max_len', 1, 64]
* custom_job_title: ['is_min_max_len', 0, 256]

The validation definition is an array with the validation function as first element, optionally followed by one or more parameters:

| Func. name | Parameters | Description |
| ---------- | ---------- | ----------- |
| is_guid    | none       | Pass if parameter is a valid GUID. |
| is_start_guid | none    | Parameter starts with a GUID. |
| is_min_max_len | min, max | Parameters is a number between _min_ and _max_. |
| is_min_len | len        | Parameter is a string of minimum _len_ characters |
| is_max_len | len        | Parameter is a string of maximum _len_ characters |
| is_empty   | none       | Parameter must be empty |
| is_not_empty | none     | Parameter must contain data |

If any validation rule fails the launch is aborted and an exception is thrown.

## Documentation and Support ##

Documentation similiar to this can be found on the [plugins Moodle page: docs.moodle.org/en/mod/ltisource_ecohub](https://docs.moodle.org/en/mod/ltisource_ecohub).

Also, a forum thread at moodle.org has been created in case questions arise or new features are requested.
The support page is located at [moodle.org/mod/forum/discuss.php?d=370762](https://moodle.org/mod/forum/discuss.php?d=370762).

_Note that a sales representative from Laerdal Medical must be contacted in order to create a plugin configuration file for an organization. See [laerdal.com](https://www.laerdal.com/) for more information._

## Contribution ##

As the plugin is created specifically for the Laerdal Medical EcoHub platform, contribution will most likely depend on knowledge hereof, but all contributions are welcome.
Laerdal Medical employees must submit an issue with the BitBucket Issue Tracker before working on a pull request.

## Copyright ##

© Laerdal Medical AS. Code for this plugin is licensed under the GPLv3 license.

Any Laerdal Medical trademarks and logos included in this plugin is property of Laerdal Medical and should not be reused, redistributed, modified, repurposed, or otherwise altered or used outside of this plugin.
