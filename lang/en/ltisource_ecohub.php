<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'ltisource_ecohub', language 'en'
 *
 * @package   ltisource_ecohub
 * @copyright 2017 Laerdal Medical
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'EcoHub LTI Source';
$string['pluginhelp'] = 'The EcoHub LTI Source plugin is like the External tool plugin. It makes it possible to add a Laerdal Medical course as an activity.';

$string['task_name'] = 'EcoHub LTI Source plugin configuration updater';

$string['cachedef_configfile'] = 'Primary configuration from plugin';
$string['cachedef_configsettings'] = 'Additional configuration, customized and/or loaded from Laerdal servers';

$string['abb_not_avaiable'] = 'N/A';

$string['config_error_title'] = 'EcoHub configuration';
$string['config_error_config_not_found'] = 'Configuration file config.json not found';
$string['config_error_file_not_found'] = 'Configuration file {$a} not found';
$string['config_error_file_not_json'] = 'Configuration file {$a} is not a valid JSON file';
$string['config_error_missing_fields'] = 'Configuration file {$a} is missing mandatory fields (version, as_domain)';
$string['config_error_file_newer'] = 'Configuration file {$a->filename} is newer ({$a->newversion}) than supported ({$a->supportedversion})';
$string['config_error_asdomain_invalid'] = 'Configuration file {$a} mandatory field as_domain is not a valid url';

$string['settings_developer_heading'] = 'Developer Mode';
$string['settings_developer_release'] = 'Plugin release: {$a}';
$string['settings_developer_file_info'] = 'Config file information';
$string['settings_developer_file_name'] = 'Full file name: {$a}';
$string['settings_developer_file_modified'] = 'Last modified: {$a}';
$string['settings_general_heading'] = 'General Settings';
$string['settings_general_readonly'] = 'The configuration file specifies a read-only configuration that cannot be edited.';
$string['settings_file_version'] = 'Config version';
$string['settings_desc_file_version'] = 'Configuration version obtained from config file';
$string['settings_site_id'] = 'Site configuration ID';
$string['settings_desc_site_id'] = 'Site identification for Laerdal EcoHub configuration repository';
$string['settings_as_domain'] = 'EcoHub access endpoint';
$string['settings_desc_as_domain'] = 'Endpoint for dynamic maintenance of configuration';
$string['settings_cs_domain'] = 'EcoHub forced Content Service';
$string['settings_desc_cs_domain'] = 'Force each launch to use this Content Service in EcoHub';
$string['settings_param_map_heading'] = 'Parameter Mapping';
$string['settings_param_map_title'] = 'LTI map entry';
$string['settings_param_map_title_warning'] = 'Evaluation error!';
$string['settings_param_map_with'] = '=';
$string['settings_param_map_description'] = 'The expression **{$a->mapping}** is evaluated and inserted into LTI parameter **{$a->param}** before the course is launched. For this user the mapping is: **{$a->expression}**';
$string['settings_param_map_description_static'] = 'The constant **{$a->mapping}** is inserted into LTI parameter **{$a->param}** before the course is launched.';
$string['settings_param_map_description_error'] = 'Error: The expression **{$a->mapping}** cannot be evaluated!';

$string['launch_enabled'] = 'Enabled';
$string['launch_enabled_desc'] = 'Must be enabled to launch LTI on Laerdal EcoHub.';

$string['cachedef_mod_ltisource_ecohub_configfile'] = 'Cache for EcoHub LTI Source configuration file location';
$string['cachedef_mod_ltisource_ecohub_configsettings'] = 'EcoHub LTI Source configuration settings cache, excluding LTI map';
$string['cachedef_mod_ltisource_ecohub_ltimap'] = 'LTI parameter map cache for EcoHub LTI Source';

$string['lti_param_invalid_error'] = 'The parameter {$a->param} with value {a->value} is not valid.';
$string['lti_param_not_allowed_error'] = 'The parameter {$a->param} must not be modified.';
$string['lti_param_invalid_empty_error'] = 'The parameter {$a} must contain a value.';
$string['lti_param_invalid_not_guid_error'] = 'The parameter {$a->param} is {$a->value}. Expected a GUID.';
$string['lti_param_invalid_not_in_range_error'] = 'The parameter {$a->param} is {$a->value} and is out of range. Range is {$a->range}.';

$string['ecohubltilaunch'] = 'An error occured during LTI launch. The parameters for EcoHub could not be resolved.';
$string['ecohubltiinvalid'] = 'An error occured during LTI launch. {$a}';

$string['privacy_statement_no_store'] = 'This plugin does not store personal data in Moodle. By default it does not send personal data to the Laerdal EcoHub servers. However, it can be configured to do so by an administrator. In case of doubt, please contact the administrator of this organization.';

$string['exception_installation_version_error'] = 'Error installing plugin, version is missing.';
$string['exception_installation_updater_task_missing'] = 'Error installing plugin, scheduled updater task was not installed.';
$string['exception_updater_invalid_response'] = 'Received invalid response from EcoHub: {$a}.';
