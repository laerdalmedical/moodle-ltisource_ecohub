<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the EcoHub hooks for the LTI Source sub-plugin.
 *
 * @package   ltisource_ecohub
 * @copyright 2017 Laerdal Medical
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once('classes/mapping.php');
require_once('classes/validation.php');

/**
 * get_shortcuts hook is called by LTI module to register sub-plugins.
 *
 * @param object $defaultitem.
 * @return array Array with one element, the sub-plugin item.
 * @throws coding_exception.
 * @throws moodle_exception.
 */
function ltisource_ecohub_get_shortcuts($defaultitem) {
    global $OUTPUT;

    $grouptitle = get_string('modulenameplural', 'mod_lti');
    $type = 'ecohub';
    $name = get_string('pluginname', 'ltisource_ecohub');

    $item = new stdClass();
    $item->title = get_string('activitytypetitle', '', (object)['activity' => $grouptitle, 'type' => $name]);
    $item->type = 'lti&type=' . $type;
    $item->name = $name;
    $item->link = new moodle_url($defaultitem->link, array('type' => $type, 'typeid' => 1234));
    $item->help = get_string('pluginhelp', 'ltisource_ecohub');
    // TODO: This simply doesn't work for some reason.Investigation undergoing.
    $item->icon = $OUTPUT->image_icon('icon', '', 'ltisource_ecohub', array('class' => 'icon'));

    $items = array($item);
    return $items;
}

/**
 * before_launch hook is called by LTI module just before signing and POST'ing LTI launch form.
 *
 * Add new LTI parameters based on config or modify existing ones just before the launch.
 * The launch cannot be cancelled gracefully so throw an exception on critical error.
 *
 *
 * @param int $instance LTI instance.
 * @param int $endpoint LTI endpoint. Cannot be modified.
 * @param int $requestparams Initial LTI parameters.
 * @return array Additional or modified LTI parameters, which is merged into the original ones.
 * @throws moodle_exception On critical error.
 */
function ltisource_ecohub_before_launch($instance, $endpoint, $requestparams) {
    global $CFG;

    $parts = parse_url($endpoint);
    function ends_with($haystack, $needle) {
        $length = strlen($needle);
        return $length === 0 ||
                (substr($haystack, -$length) === $needle);
    };
    // Require valid, secure and contentservice.net endpoint to engage plugin.
    if ($parts === false ||
            $parts['scheme'] !== 'https' ||
            !ends_with($parts['host'], 'contentservice.net')) {
        return array();
    }

    $config = cache::make('ltisource_ecohub', 'configsettings');

    $ecohubmap = ltisource_ecohub_mapping::get_instance();
    $ecohubmap->set_map_var('lti', function($mapvar) use ($requestparams) {
        if (is_array($requestparams) && array_key_exists($mapvar, $requestparams)) {
            return $requestparams[$mapvar];
        }
        return false;
    });

    $settings = $ecohubmap->get_expanded_settings($config->get('ltimap'));
    $ecohubmap->set_map_var('lti');
    if ($settings === false) {
        throw new moodle_exception('ecohubltilaunch',
                'ltisource_ecohub',
                $CFG->wwwroot . '/admin/settings.php?section=ltisourcesettingecohub');
        // TODO: Need to think of something that makes EcoHub complain as we cannot prevent the launch.
    }
    $validator = ltisource_ecohub_validation::get_instance();
    if (($result = $validator->validate_lti_parameters($settings)) !== true) {
        throw new moodle_exception('ecohubltiinvalid',
                'ltisource_ecohub',
                $CFG->wwwroot . '/admin/settings.php?section=ltisourcesettingecohub',
                $result);
    }
    return $settings;
}

