<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the EcoHub hooks for the LTI Source sub-plugin.
 *
 * @package   ltisource_ecohub
 * @copyright 2018 Laerdal Medical
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../classes/storage.php');

function xmldb_ltisource_ecohub_upgrade($oldversion) {
    // Add plugin release in database for v1.1 and on.
    $plugin = new stdClass();
    $plugin->release = null;
    require(__DIR__ . '/../version.php');
    if (empty($plugin->release)) {
        return false;
    }
    $dbstorage = new ltisource_ecohub_db_storage();
    $dbstorage->set_plugin_release($plugin->release);

    return true;
}