<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/../classes/ecohub_cache_state.php');

// Cache definitions.

// Due to MUC not being updated when uninstalling, do not define cache when doing so.
if (ecohub_cache_state::is_state(ecohub_cache_state::STATE_PLUGIN_UNINSTALLING)) {
    return;
}

$definitions = array(
    // Configuration file cache.
    'configfile' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'simplekeys' => true,
        'simpledata' => true,
        'datasource' => 'ecohub_configfile_cache_data_source',
        'datasourcefile' => 'mod/lti/source/ecohub/classes/ecohub_cache_data_source.php',
        'staticacceleration' => true,
        'staticaccelerationsize' => 2,
        'ttl' => 3600,
        'invalidationevents' => array(
                'ecohubconfigchanged',
        )
    ),
    // General and mapping settings within configuration file/db.
    'configsettings' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'simplekeys' => true,
        'simpledata' => true,
        'overrideclass' => 'ecohub_settings_cache_data_loader',
        'overrideclassfile' => 'mod/lti/source/ecohub/classes/ecohub_cache_data_loader.php',
        'staticacceleration' => true,
        'staticaccelerationsize' => 2,
        'ttl' => 3600,
    )
);
