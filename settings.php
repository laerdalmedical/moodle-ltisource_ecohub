<?php
// This file is part of the Laerdal EcoHub LTI Source sub-plugin.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

require_once('lib.php');
require_once('classes/ecohub_setting_configtext.php');
require_once('classes/ecohub_setting_text.php');
require_once('classes/ecohub_setting_configmapping.php');

if ($ADMIN->fulltree) {
    global $CFG;

    if (!function_exists("text")) {
        /**
         * @param string $identifier Text identifier
         * @param object $a Possible text substitution.
         * @return string
         */
        function text($identifier, $a = null) {
            try {
                return get_string($identifier, 'ltisource_ecohub', $a);
            } catch (exception $e) {
                return 'String not found: ' . $identifier;
            }
        }
    }

    $configfilecache = cache::make('ltisource_ecohub', 'configfile');
    $configsettingscache = cache::make('ltisource_ecohub', 'configsettings');

    if ($CFG->debugdeveloper) {
        $settings->add(new admin_setting_heading(
                'ecohub/developermode',
                text('settings_developer_heading'),
                ''
        ));
        $settings->add(new ecohub_setting_text(
                'ecohub/release',
                text('settings_developer_release', $configsettingscache->get('_pluginrelease')),
                ''
        ));
        $settings->add(new ecohub_setting_text(
                'ecohub/configinfo',
                text('settings_developer_file_info'),
                ''
        ));
        $settings->add(new ecohub_setting_text(
                'ecohub/filename',
                text('settings_developer_file_name', $configfilecache->get('filename')),
                ''
        ));
        $timestamp = $configfilecache->get('timestamp');
        $timestamptext = $timestamp !== false ?
                date("Y-m-d H:i:s", $timestamp) :
                text('abb_not_avaiable');
        $settings->add(new ecohub_setting_text(
                'ecohub/timestamptext',
                text('settings_developer_file_modified', $timestamptext),
                ''
        ));
    }

    if ($configfilecache->get('isvalid')) {
        $settings->add(new admin_setting_heading(
                'ecohub/generalsettings',
                text('settings_general_heading'),
                ''
        ));

        $readonly = $configsettingscache->get('readonly');
        $readonly = $readonly !== '0';
        if ($readonly) {
            $settings->add(new ecohub_setting_text(
                    'ecohub/readonly',
                    text('settings_general_readonly'),
                    ''
            ));
        }

        $version = new ecohub_setting_configtext('version', text('settings_file_version'),
                text('settings_desc_file_version'),
                null, PARAM_TEXT, 8);
        $version->readonly(true);
        $settings->add($version);

        $siteid = new ecohub_setting_configtext('siteid',  text('settings_site_id'),
                text('settings_desc_site_id'),
                null, PARAM_TEXT, 64);
        // If no site id is configured, add the possibility to add one, no matter what.
        if ($siteid->readonly()) {
            $id = $configsettingscache->get('siteid');
            if (!isset($id)) {
                $siteid->readonly(false);
            }
        }
        $settings->add($siteid);

        $settings->add(new ecohub_setting_configtext('asdomain', text('settings_as_domain'),
                text('settings_desc_as_domain'),
                null, PARAM_URL, 64));

        // This does not seem to be possible yet!
        if (false) {
            $settings->add(new ecohub_setting_configtext('csdomain', text('settings_cs_domain'),
                    text('settings_desc_cs_domain'), null, PARAM_URL, 64));
        }

        $settings->add(new admin_setting_heading(
                'ecohub/mapsettings',
                text('settings_param_map_heading'),
                ''
        ));

        foreach ($configsettingscache->get('ltimap') as $param => $map) {
            try {
                $settings->add(new ecohub_setting_configmapping('ltimap/' . $param));
            } catch (coding_exception $e) {
                break;
            }
        }
    }
}
